/**
 * Created with JetBrains WebStorm.
 * User: go
 * Date: 2012/12/16
 * Time: 21:06
 * To change this template use File | Settings | File Templates.
 */

function multiByTwo(a, b, c) {
    var i, ar = [];

    for (i = 0; i < 3; i++) {

        ar[i] = arguments[i] * 2;
    }

    return ar;
}

function addOne(a) {
    return a + 1;
}

var myarr = [];

console.log(multiByTwo(1, 2, 3));

console.log(addOne(100));

myarr = multiByTwo(10, 20, 30);

console.log(myarr);

(function () {
    for (var i = 0; i < 3; i++) {
        myarr[i] = addOne(myarr[i]);
    }
})();

console.log(myarr);

function multiPlayByTow(a, b, c, callback) {
    var i, ar = [];
    for (i = 0; i < 3; i++) {
        ar[i] = callback(arguments[i] * 2);
    }
    return ar;
}

multiPlayByTow(1, 2, 3, addOne);
console.log(myarr);


(function (name) {
    alert('Hello　' + name + '!');
})('double');


var a = function () {

    function someSetup() {
        var setup = 'done';
    }

    function actualWork() {
        alert('Worky-worky');
    }

    someSetup();

    return actualWork();
}();

