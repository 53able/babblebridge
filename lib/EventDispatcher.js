/**
 * Created with JetBrains WebStorm.
 * User: go
 * Date: 2012/11/07
 * Time: 11:50
 * To change this template use File | Settings | File Templates.
 */
function EventDispatcher() {
}

/**
 EventDispatcher.initialize( obj )

 オブジェクトobjに、W3C DOM3互換のイベント通知機能を追加します。
 これによりobjは、onLoad等のイベントを発行することができます。
 オブジェクトobjはこれにより、以下のプロパティと関数を装備します。
 obj.addEventListener( イベント名, リスナーオブジェ);
 obj.removeEventListener(　イベント名, リスナーオブジェクト);
 obj.dispatchEvent( Eventオブジェクト);

 @param obj    オブジェクト
 */


EventDispatcher.initialize = function (obj) {
//MIX IN following property and function int obj
//be careful about Name Scape Corrision just for incase
    obj.__ed_eventContainer = new Object();
    obj.addEventListener = this._addEventListener;
    obj.removeEventListener = this._removeEventListener;
    obj.dispatchEvent = this._dispatchEvent;
}

/*
 CAUTION INTERNAL OBJECT	do not call it directory from EventDispatcher
 refered from targetObject.addEventListener()
 */

EventDispatcher._addEventListener = function (eventName, object) {
//CAUTION
//third param is not implemented yet

//CAUTION:
//scope of this object is always "TARGET" OBJECT, not EventDispatcher
    if (this.__ed_eventContainer[eventName] == null) {
        this.__ed_eventContainer[eventName] = new Array();
    }
    this.removeEventListener(eventName, object);
    this.__ed_eventContainer[eventName].push(object);	//register object
}


/*
 CAUTION INTERNAL OBJECT	do not call it directory from EventDispatcher

 refered from targetObject.removeEventListener()
 */

EventDispatcher._removeEventListener = function (eventName, object) {
//CAUTION:
//scope of this object is always "TARGET" OBJECT, not EventDispatcher
    var listener_ar = this.__ed_eventContainer[eventName];
    if (listener_ar == undefined) return;

    var imax = listener_ar.length;
    for (var i = 0; i var listener = listener_ar[i];
    if (listener == object) {
        listener_ar.splice(i, 1);
        return;
    }
}


/*
 CAUTION INTERNAL OBJECT	do not call it directory from EventDispatcher
 refered from targetObject.dispatchEvent()
 */

EventDispatcher._dispatchEvent = function (eventObj) {
//CAUTION:
//scope of this object is always "TARGET" OBJECT,not EventDispatcher
    if (eventObj.target == null) eventObj.target = this;

    var eventName = eventObj.type;
    if (this.__ed_eventContainer[eventName] == null) return;

    var imax = this.__ed_eventContainer[eventName].length;
    for (var i = 0; i var listener = this.__ed_eventContainer[eventName][i];
    if (typeof(listener) == "object") {
        listener[eventName].apply(listener, new Array(eventObj));
        return;
    } else {
        listener(eventObj);
        return;
    }
}
