feel = 'cool'
color = switch feel
  when 'warm' then 'red'
  when 'cool' then 'blue'
  else
    'white'
console.log color