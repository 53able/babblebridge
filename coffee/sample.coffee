###
  どんなときにCoffeeScriptを使えばいいのか
  どのような開発でもCoffeeScriptによるメリットを受けることができますが、特に大規模な開発プロジェクトで有効です。
  ソースコードの量が増えれば増えるほど、JavaScriptに比べて工数が減る、全体の見通しが良くなるといったメリットが大きくなります。
  まずは試しにCoffeeScriptを使って見ることをおすすめします。もし既存のプロジェクトがあれば、その中のたった一つのファイルにだけでもCoffeeScriptを導入できます。
  お気に召さなければJavaScriptに戻せるので、ぜひ気軽にトライしみてください。
###

say = (text) ->
  console.log text

say "hello!"

Math.max Math.min 5, 7, 2

do Math.random

hello = ->
  console.log "Hello!"

hello = (neme) ->
  cosole.log "hello, #{name}!"

getFullName = (firstName, lastName) ->
  "#{firstName}#{lastName}"

getPrice = (price, discountPercent = 0) ->
  price * (1 - discountPercent / 100)

getPrice 1000
getPrice 1000, 30

setTimeout ->
  console.log "Timed out!"
, 1000

#24
name = "Nao"
console.log "Hello,#{name}!"
console.log "1+1 = #{1 + 1}"

opening = """fdskfjdksajflkajfd;safjdsalk
          fdsjfkdlasjffsd
          fsdafkjdlsajflkas
          fasdfaklfjda"""

colors = [
  'red'
  "yellow"
  "green"
]

numpad = [
  7, 8, 9
  4, 5, 6
  1, 2, 3
]

shapes =
  shapeA:
    x: 0
    y: 0
    width: 15
    height: 15
  shapeB:
    x: 15
    y: 0
    width: 0
    height: 30

response = {status: 200, content: '受け付けました'}
User.update({name: 'Nao Iizuka', cuntry: 'Japan'})

response = status: 200, content: '受け付けました'

commands = save: 100, delete: 200

###
  変数スコープ
  使った変数はコンパイル時に自動的に変数宣言が追加されます。
  変数が代入された場所によって、宣言が挿入される箇所が決まります。
###
total = 0

add = ->
  price = 30
  total += price

###
条件分岐
###

if money < 1000
  do work
else
  do buy

if money < 1000 then do work else do buy

#後置形式のif
do eat if hungry

#unless
unless fun
  do changeSomething

#while
count = 0
while count < 10
  count++
console.log count

count = 0
count++ while count < 10
console.log count

#loop
count = 0
loop
  console.log count
  if ++count > 3
    break


#switch
switch season
  when 'haru' then go 'お花見'
  when 'natu' then go 'umi'
  when 'aki' then go 'kouyougari'
  when 'fuyu'
    setup 'kotatu'
    eat 'mikan'
  when 'bon','shougatu'
    go 'jikka'
  else go 'sanpo'

#条件分岐のある関数の戻り値
isAdult = (age) ->
  if age < 20
    false
  else
    true

#配列に他するループ
fruits = ['apple','orange','banana']
for fruit in fruits
  console.log fruit

fruits = ['apple','orange','banana']
for fruit,i in fruits
  console.log "#{i}:#{fruit}"
